//
//  ssh2-caller.c
//  TreeView1
//
//  Created by Nini on 21/09/14.
//  Copyright (c) 2014 Nini. All rights reserved.
//

#include "ssh2-caller.h"

#include <libssh2.h>
#include <libssh2_sftp.h>

#include <sys/socket.h>
#include <netinet/in.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <arpa/inet.h>


#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>

const char *username="username";
const char *password="password";

void ssh_disconnect(LIBSSH2_SESSION* session)
{
    libssh2_session_disconnect(session,
                               
                               "Normal Shutdown, Thank you for playing");
    
    fprintf(stderr, "all done!\n");
    
    libssh2_exit();
    
    
    libssh2_session_free(session);
}

void ssh_channel_disconnect(LIBSSH2_CHANNEL* chan)
{
    libssh2_channel_free(chan);
}

LIBSSH2_CHANNEL* ssh_connect(char host[256], int port, const char* username, const char* password)
{
    unsigned long hostaddr;
    int rc, sock, i, auth_pw = 0;
    struct sockaddr_in sin;
    const char *fingerprint;
    char *userauthlist;
    LIBSSH2_SESSION *session;
    LIBSSH2_CHANNEL *channel;
    
    hostaddr = inet_addr(host);
    
    rc = libssh2_init(0);
    if (rc != 0) return NULL;
    
    /* Ultra basic "connect to port 22 on localhost".  Your code is
     * responsible for creating the socket establishing the connection
     */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    
    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = hostaddr;
    if (connect(sock, (struct sockaddr*)(&sin),
                sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "failed to connect!\n");
        return NULL;
    }
    
    /* Create a session instance and start it up. This will trade welcome
     * banners, exchange keys, and setup crypto, compression, and MAC layers
     */
    session = libssh2_session_init();
    
    if (libssh2_session_handshake(session, sock)) {
        
        fprintf(stderr, "Failure establishing SSH session\n");
        return NULL;
    }
    
    /* At this point we havn't authenticated. The first thing to do is check
     * the hostkey's fingerprint against our known hosts Your app may have it
     * hard coded, may go to a file, may present it to the user, that's your
     * call
     */
    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);
    
    fprintf(stderr, "Fingerprint: ");
    for(i = 0; i < 20; i++) {
        fprintf(stderr, "%02X ", (unsigned char)fingerprint[i]);
    }
    fprintf(stderr, "\n");
    
    /* check what authentication methods are available */
    userauthlist = libssh2_userauth_list(session, username, strlen(username));
    
    fprintf(stderr, "Authentication methods: %s\n", userauthlist);
    if (strstr(userauthlist, "password") != NULL) {
        auth_pw |= 1;
    }
    if (strstr(userauthlist, "keyboard-interactive") != NULL) {
        auth_pw |= 2;
    }
    if (strstr(userauthlist, "publickey") != NULL) {
        auth_pw |= 4;
    }
    if (auth_pw & 1) {
        /* We could authenticate via password */
        if (libssh2_userauth_password(session, username, password)) {
            
            fprintf(stderr, "\tAuthentication by password failed!\n");
            goto shutdown;
        } else {
            fprintf(stderr, "\tAuthentication by password succeeded.\n");
        }
    }
    
    /* Request a shell */
    if (!(channel = libssh2_channel_open_session(session))) {
        
        fprintf(stderr, "Unable to open a session\n");
        goto shutdown;
    }
    
    
    /* Request a terminal with 'vanilla' terminal emulation
     * See /etc/termcap for more options
     */
    if (libssh2_channel_request_pty(channel, "vanilla")) {
        
        fprintf(stderr, "Failed requesting pty\n");
        goto skip_shell;
    }
    
    /* Open a SHELL on that pty */
    if (libssh2_channel_shell(channel)) {
        
        fprintf(stderr, "Unable to request shell on allocated pty\n");
        goto shutdown;
    }
    
skip_shell:
    if (channel) {
        libssh2_channel_free(channel);
        
        channel = NULL;
    }
    
    /* Other channel types are supported via:
     * libssh2_scp_send()
     * libssh2_scp_recv()
     * libssh2_channel_direct_tcpip()
     */
    
shutdown:
    ssh_disconnect(session);
    if(channel) ssh_channel_disconnect(channel);
    
    return NULL;
}



char* ssh_read(LIBSSH2_CHANNEL* chan)
{
    static char buffer[16384];
    memset(buffer,0,sizeof(buffer));
    
    libssh2_channel_read_ex(chan, 0, buffer, 16384);
    return buffer;
}

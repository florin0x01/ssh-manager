//
//  AppDelegate.swift
//  TreeView1
//
//  Created by Nini on 17/09/14.
//  Copyright (c) 2014 Nini. All rights reserved.
//

import Cocoa
import AppKit

class AppDelegate: NSObject, NSApplicationDelegate {


    @IBOutlet var parentWindow: NSView!
    @IBOutlet var hudWindow: NSWindow!
    @IBOutlet var txtView: NSTextView!
    
    @IBOutlet weak var button1: NSButton!
    
    @IBAction func button1_click(sender: AnyObject) {
        var alert = NSAlert()
        alert.messageText = "Hello"
        alert.runModal()
    }
   
    func checkTaskStatus(var notif: NSNotification) {
        var status = notif.object?.terminationStatus
        var name = notif.name
        txtView.insertText("Termination status: \(status)")
    }
    
    func applicationDidFinishLaunching(aNotification: NSNotification?) {
        // Insert code here to initialize your application
        
        var pid = NSProcessInfo.processInfo().processIdentifier
        
        println("PID: \(pid)")
        var th = TaskHandler()
        var id:uint32 = 0
        
        button1.title = "fdFDGDFG"
        
        th.addTask("/bin/bash", arguments: [], textControl: txtView, id: id)
      //  var message = th.getMessage(0)
        
        //println("Got message: \(message)")
    }

    func applicationWillTerminate(aNotification: NSNotification?) {
        // Insert code here to tear down your application
    }

    
}


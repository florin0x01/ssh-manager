//
//  ConnectionViewDataSource.swift
//  TestApp
//
//  Created by Nini on 13/09/14.
//  Copyright (c) 2014 Nini. All rights reserved.
//

import Foundation
import AppKit

class ConnectionViewDataSource: NSObject, NSOutlineViewDataSource
{
    @IBOutlet var sshView: NSTextView!
    override init()
    {
        self.items = []
        
        super.init()
        
        println("Init ConnViewDS")
        
        var root1 = ConnectionViewItem()
        root1.name = "VPS"
        
        var ci : ConnectionViewItem
        
        ci = root1.add("Pre-Production")
        ci.add("Item1")
        ci.add("Item2")
        
        root1.add("no37.ro")
        root1.add("livin.co")
        root1.add("193.226.51.1")
        
        var it = root1.children.objectAtIndex(0)
        
        var root2 = ConnectionViewItem()
        root2.name = "Dedicated"
        root2.add("Dallas")
        root2.add("Texas")
        
        var root3 = ConnectionViewItem()
        root3.name = "Hosting"
        root3.add("1and1.de");
        root3.add("godaddy.com")
        
        self.items.addObject(root1)
        self.items.addObject(root2)
        self.items.addObject(root3)
    }
    
    func outlineView(outlineView: NSOutlineView!, numberOfChildrenOfItem item: AnyObject!) -> Int
    {
        println("c1")
        var count = (item != nil ? (item as ConnectionViewItem).children.count : self.items.count)
        println("Count: \(count)")
        return count
    }
    
    func outlineView(outlineView: NSOutlineView!, isItemExpandable item: AnyObject!) -> Bool
    {
        println("C2")
        return (item == nil) ? true : ((item as ConnectionViewItem).children.count != 0 ? true : false)
    }
    
    func outlineView(outlineView: NSOutlineView!, child index: Int, ofItem item: AnyObject!) -> AnyObject!
    {
        println("Called3")
        
        println("Index is \(index)")
        if item == nil {
            return self.items.objectAtIndex(index)
        }
        
        return (item as ConnectionViewItem).children.objectAtIndex(index)
    }
    
    func outlineView(outlineView: NSOutlineView!, objectValueForTableColumn tableColumn: NSTableColumn!, byItem item: AnyObject!) -> AnyObject!
    {
        println("c4")
        
        if item == nil {
            println("Item is nil")
            return self.items.objectAtIndex(0).name
        }
        
        var name = (item as ConnectionViewItem).name
        println ("Name is " + name)
        
        //  sshView.insertText(item.name)
        
        return item.name
    }
    
    
    
    var items: NSMutableArray
    
    
    //TODO need to implement
    //optional func outlineView(outlineView: NSOutlineView!, viewForTableColumn tableColumn: NSTableColumn!, //item:AnyObject!) -> NSView!
    
    //from delegate
}

//
//  ConnectionViewItem.swift
//  TestApp
//
//  Created by Nini on 13/09/14.
//  Copyright (c) 2014 Nini. All rights reserved.
//

import Foundation
import AppKit

class ConnectionViewItem: NSObject
{
    override init()
    {
        name = ""
        parent = nil
        self.children = NSMutableArray()
        self.icon = NSImage(named: "mac_icon")
        
        if self.icon == nil{
            println("Icon nil")
        }
    }
    
    var parent:ConnectionViewItem!
    var name:NSString
    
    func count() -> Int
    {
        return self.children.count
    }
    
    func compareItems(other:AnyObject!, first: AnyObject!) -> NSComparisonResult
    {
        
        return (other as ConnectionViewItem).name.compare((first as ConnectionViewItem).name)
    }
    
    func add(var name:String) -> ConnectionViewItem
    {
        println("add conviewitem \(name)")
        var child = ConnectionViewItem()
        child.name = name
        child.parent = self
        child.parent.icon = NSImage(named: "folder_icon")
        
        self.children.addObject(child)
        
        return child
    }
    
    var children : NSMutableArray
    var icon: NSImage!
}